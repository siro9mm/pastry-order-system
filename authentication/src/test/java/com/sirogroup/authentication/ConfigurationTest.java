package com.sirogroup.authentication;

import org.restexpress.util.Environment;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class ConfigurationTest {

    Configuration config;

    @BeforeMethod
    public void beforeTest() throws Exception {
        String[] args = { "dev", "dev" };
        config = Environment.load(args, Configuration.class);
    }

    @AfterMethod
    public void afterTest() {

    }

    @Test
    public void testFillValues() {

    }

    @Test
    public void testGetPort() {
        Assert.assertEquals(8081, config.getPort());
    }

}
