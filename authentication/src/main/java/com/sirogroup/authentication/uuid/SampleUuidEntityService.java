package com.sirogroup.authentication.uuid;

import java.util.List;

import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.strategicgains.repoexpress.domain.Identifier;
import com.strategicgains.syntaxe.ValidationEngine;

/**
 * This is the 'service' or 'business logic' layer, where business logic,
 * syntactic and semantic domain validation occurs, along with calls to the
 * persistence layer.
 */
public class SampleUuidEntityService {

    private SampleUuidEntityRepository samples;

    private static final Logger LOG = LoggerFactory.getLogger(SampleUuidEntityService.class);

    public SampleUuidEntityService(SampleUuidEntityRepository samplesRepository) {
        super();
        this.samples = samplesRepository;
    }

    public SampleUuidEntity create(SampleUuidEntity entity) {
        LOG.info("[ENTER] create()");
        ValidationEngine.validateAndThrow(entity);
        LOG.info("[EXIT] create()");
        return samples.create(entity);
    }

    public SampleUuidEntity read(Identifier id) {
        LOG.info("[ENTER] read()");
        LOG.info("[EXIT] read()");
        return samples.read(id);
    }

    public void update(SampleUuidEntity entity) {
        LOG.info("[ENTER] update()");
        ValidationEngine.validateAndThrow(entity);
        LOG.info("[EXIT] update()");
        samples.update(entity);
    }

    public void delete(Identifier id) {
        LOG.info("[ENTER] delete()");
        LOG.info("[EXIT] delete()");
        samples.delete(id);
    }

    public List<SampleUuidEntity> readAll(QueryFilter filter, QueryRange range, QueryOrder order) {
        LOG.info("[ENTER] readAll()");
        LOG.info("[EXIT] readAll()");
        return samples.readAll(filter, range, order);
    }

    public long count(QueryFilter filter) {
        LOG.info("[ENTER] count()");
        LOG.info("[EXIT] count()");
        return samples.count(filter);
    }
}
