package com.sirogroup.authentication.serialization;

import org.restexpress.serialization.xml.XstreamXmlProcessor;

import com.sirogroup.authentication.uuid.SampleUuidEntity;

public class XmlSerializationProcessor extends XstreamXmlProcessor {
    public XmlSerializationProcessor() {
        super();
        alias("sample", SampleUuidEntity.class);
        // alias("element_name", Element.class);
        // alias("element_name", Element.class);
        // alias("element_name", Element.class);
        // alias("element_name", Element.class);
        registerConverter(new XstreamUuidConverter());
        registerConverter(new XstreamOidConverter());
    }
}
